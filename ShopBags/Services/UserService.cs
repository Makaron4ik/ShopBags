﻿using ShopBags.Helpers;
using ShopBags.Sessions;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ShopBags.Services
{
    internal class UserService
    {
        private const string DB_SERVER = "DESKTOP-JVC7GNS\\VOVAN_MSSQLSERVE";
        private const string DB_NAME = "ShopBags";

        private static string connectionString = $"Server={DB_SERVER};Database={DB_NAME};Trusted_Connection=True;";

        public bool AuthenticateUser(string email, string password)
        {
            string query = $"SELECT id as 'Id', username as 'Name', email as 'Email', isAdmin as 'IsAdmin', isEditor as 'IsEditor' FROM Users WHERE email = @Email AND password = @Password AND isActive = 1 ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@Password", password);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                         
                            reader.Read();
                            int userId = reader.GetInt32(reader.GetOrdinal("Id"));
                            string userName = reader.GetString(reader.GetOrdinal("Name"));
                            string userEmail = reader.GetString(reader.GetOrdinal("Email"));
                            bool isAdmin = reader.GetBoolean(reader.GetOrdinal("IsAdmin"));
                            bool isEditor = reader.GetBoolean(reader.GetOrdinal("IsEditor"));

                            UserSession.Instance.SetUserData(userId, userName, userEmail, isAdmin, isEditor);

                            return true;
                        }
                        else
                        {
                            return false; 
                        }
                    }
                }
            }
        }

        public bool GetUserOrders()
        {
            string query = $"SELECT COUNT(*) as counter FROM ORDERS WHERE fk_user_id = {UserSession.Instance.id}";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            
                            reader.Read();
                            int counter = reader.GetInt32(reader.GetOrdinal("counter"));

                            UserSession.Instance.SetUserOrders(counter);

                            return true;
                        }
                        else
                        {
                            return false; 
                        }
                    }
                }
            }
        }

        public string RegisterUser(string username, string password, string email)
        {
            
            if (!IsEmailUnique(email))
            {
                return "Email is not unique"; 
            }

            string query = "INSERT INTO Users (username, password, email, isActive) VALUES (@Username, @Password, @Email, @IsActive)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Username", username);
                    command.Parameters.AddWithValue("@Password", password);
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@IsActive", 1);

                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        
                        return "Registration successful!";
                    }
                    else
                    {
                        return "Registration failed";
                    }
                }
            }
        }
        
        private bool IsEmailUnique(string email)
        {
            string query = "SELECT COUNT(*) FROM Users WHERE Email = @Email";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Email", email);

                    int count = (int)command.ExecuteScalar();

                    return count == 0;
                }
            }
        }
    }
}
