﻿namespace ShopBags.Views
{
    partial class StoreView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StoreView));
            dgvStore = new DataGridView();
            Action = new DataGridViewButtonColumn();
            panel1 = new Panel();
            lblOrdersCounter = new Label();
            btnShowCart = new Button();
            tableLayoutPanel1 = new TableLayoutPanel();
            btnApplyFilters = new Button();
            txtNameFilter = new TextBox();
            cbSizeFilter = new ComboBox();
            label4 = new Label();
            cbBrandFilter = new ComboBox();
            label3 = new Label();
            cbCategoryFilter = new ComboBox();
            label2 = new Label();
            label1 = new Label();
            panel2 = new Panel();
            pictureBoxRefresh = new PictureBox();
            btnPanel = new Button();
            imgUserIcon = new PictureBox();
            lblEmail = new Label();
            lblUsername = new Label();
            btnResetFilters = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvStore).BeginInit();
            panel1.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxRefresh).BeginInit();
            ((System.ComponentModel.ISupportInitialize)imgUserIcon).BeginInit();
            SuspendLayout();
            // 
            // dgvStore
            // 
            dgvStore.BackgroundColor = SystemColors.ButtonFace;
            dgvStore.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = SystemColors.ButtonFace;
            dataGridViewCellStyle1.Font = new Font("Segoe UI", 9F);
            dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            dgvStore.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dgvStore.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvStore.Columns.AddRange(new DataGridViewColumn[] { Action });
            dgvStore.Dock = DockStyle.Right;
            dgvStore.GridColor = Color.Gray;
            dgvStore.Location = new Point(226, 0);
            dgvStore.Margin = new Padding(3, 4, 3, 4);
            dgvStore.Name = "dgvStore";
            dgvStore.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dgvStore.RowHeadersWidth = 51;
            dgvStore.RowTemplate.Height = 30;
            dgvStore.Size = new Size(1219, 908);
            dgvStore.TabIndex = 0;
            dgvStore.CellContentClick += dgvStore_CellContentClick;
            // 
            // Action
            // 
            Action.HeaderText = "Action";
            Action.MinimumWidth = 6;
            Action.Name = "Action";
            Action.Text = "Buy";
            Action.UseColumnTextForButtonValue = true;
            Action.Width = 125;
            // 
            // panel1
            // 
            panel1.Controls.Add(lblOrdersCounter);
            panel1.Controls.Add(btnShowCart);
            panel1.Controls.Add(tableLayoutPanel1);
            panel1.Controls.Add(panel2);
            panel1.Controls.Add(btnResetFilters);
            panel1.Dock = DockStyle.Bottom;
            panel1.Location = new Point(0, 0);
            panel1.Margin = new Padding(3, 4, 3, 4);
            panel1.Name = "panel1";
            panel1.Size = new Size(226, 908);
            panel1.TabIndex = 1;
            // 
            // lblOrdersCounter
            // 
            lblOrdersCounter.AutoSize = true;
            lblOrdersCounter.BackColor = Color.Red;
            lblOrdersCounter.Font = new Font("Segoe UI", 24F);
            lblOrdersCounter.ForeColor = Color.White;
            lblOrdersCounter.Location = new Point(14, 784);
            lblOrdersCounter.Name = "lblOrdersCounter";
            lblOrdersCounter.Padding = new Padding(3, 4, 3, 4);
            lblOrdersCounter.Size = new Size(51, 62);
            lblOrdersCounter.TabIndex = 2;
            lblOrdersCounter.Text = "1";
            // 
            // btnShowCart
            // 
            btnShowCart.BackgroundImage = Assets.Resource.icon_cart;
            btnShowCart.BackgroundImageLayout = ImageLayout.Zoom;
            btnShowCart.Dock = DockStyle.Bottom;
            btnShowCart.Location = new Point(0, 829);
            btnShowCart.Margin = new Padding(11, 13, 11, 13);
            btnShowCart.Name = "btnShowCart";
            btnShowCart.Padding = new Padding(11, 13, 11, 13);
            btnShowCart.Size = new Size(226, 79);
            btnShowCart.TabIndex = 5;
            btnShowCart.UseVisualStyleBackColor = true;
            btnShowCart.Click += btnShowCart_Click;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(btnApplyFilters, 0, 9);
            tableLayoutPanel1.Controls.Add(txtNameFilter, 0, 2);
            tableLayoutPanel1.Controls.Add(cbSizeFilter, 0, 8);
            tableLayoutPanel1.Controls.Add(label4, 0, 7);
            tableLayoutPanel1.Controls.Add(cbBrandFilter, 0, 6);
            tableLayoutPanel1.Controls.Add(label3, 0, 5);
            tableLayoutPanel1.Controls.Add(cbCategoryFilter, 0, 4);
            tableLayoutPanel1.Controls.Add(label2, 0, 3);
            tableLayoutPanel1.Controls.Add(label1, 0, 1);
            tableLayoutPanel1.Dock = DockStyle.Top;
            tableLayoutPanel1.Location = new Point(0, 147);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 10;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 10F));
            tableLayoutPanel1.Size = new Size(226, 377);
            tableLayoutPanel1.TabIndex = 4;
            // 
            // btnApplyFilters
            // 
            btnApplyFilters.Dock = DockStyle.Fill;
            btnApplyFilters.Location = new Point(3, 337);
            btnApplyFilters.Margin = new Padding(3, 4, 3, 4);
            btnApplyFilters.Name = "btnApplyFilters";
            btnApplyFilters.Size = new Size(220, 36);
            btnApplyFilters.TabIndex = 6;
            btnApplyFilters.Text = "Filter";
            btnApplyFilters.UseVisualStyleBackColor = true;
            btnApplyFilters.Click += btnApplyFilters_Click;
            // 
            // txtNameFilter
            // 
            txtNameFilter.Dock = DockStyle.Fill;
            txtNameFilter.Location = new Point(3, 78);
            txtNameFilter.Margin = new Padding(3, 4, 3, 4);
            txtNameFilter.Name = "txtNameFilter";
            txtNameFilter.Size = new Size(220, 27);
            txtNameFilter.TabIndex = 7;
            // 
            // cbSizeFilter
            // 
            cbSizeFilter.Dock = DockStyle.Fill;
            cbSizeFilter.FormattingEnabled = true;
            cbSizeFilter.Location = new Point(3, 300);
            cbSizeFilter.Margin = new Padding(3, 4, 3, 4);
            cbSizeFilter.Name = "cbSizeFilter";
            cbSizeFilter.Size = new Size(220, 28);
            cbSizeFilter.TabIndex = 10;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Dock = DockStyle.Fill;
            label4.Location = new Point(3, 259);
            label4.Name = "label4";
            label4.Size = new Size(220, 37);
            label4.TabIndex = 14;
            label4.Text = "Size";
            label4.TextAlign = ContentAlignment.BottomLeft;
            // 
            // cbBrandFilter
            // 
            cbBrandFilter.Dock = DockStyle.Fill;
            cbBrandFilter.FormattingEnabled = true;
            cbBrandFilter.Location = new Point(3, 226);
            cbBrandFilter.Margin = new Padding(3, 4, 3, 4);
            cbBrandFilter.Name = "cbBrandFilter";
            cbBrandFilter.Size = new Size(220, 28);
            cbBrandFilter.TabIndex = 9;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Dock = DockStyle.Fill;
            label3.Location = new Point(3, 185);
            label3.Name = "label3";
            label3.Size = new Size(220, 37);
            label3.TabIndex = 13;
            label3.Text = "Brand";
            label3.TextAlign = ContentAlignment.BottomLeft;
            // 
            // cbCategoryFilter
            // 
            cbCategoryFilter.Dock = DockStyle.Fill;
            cbCategoryFilter.FormattingEnabled = true;
            cbCategoryFilter.Location = new Point(3, 152);
            cbCategoryFilter.Margin = new Padding(3, 4, 3, 4);
            cbCategoryFilter.Name = "cbCategoryFilter";
            cbCategoryFilter.Size = new Size(220, 28);
            cbCategoryFilter.TabIndex = 8;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Dock = DockStyle.Fill;
            label2.Location = new Point(3, 111);
            label2.Name = "label2";
            label2.Size = new Size(220, 37);
            label2.TabIndex = 12;
            label2.Text = "Category";
            label2.TextAlign = ContentAlignment.BottomLeft;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Dock = DockStyle.Fill;
            label1.Location = new Point(3, 37);
            label1.Name = "label1";
            label1.Size = new Size(220, 37);
            label1.TabIndex = 11;
            label1.Text = "Bag name";
            label1.TextAlign = ContentAlignment.BottomLeft;
            // 
            // panel2
            // 
            panel2.BackColor = Color.Teal;
            panel2.Controls.Add(pictureBoxRefresh);
            panel2.Controls.Add(btnPanel);
            panel2.Controls.Add(imgUserIcon);
            panel2.Controls.Add(lblEmail);
            panel2.Controls.Add(lblUsername);
            panel2.Dock = DockStyle.Top;
            panel2.Location = new Point(0, 0);
            panel2.Margin = new Padding(3, 4, 3, 4);
            panel2.Name = "panel2";
            panel2.Size = new Size(226, 147);
            panel2.TabIndex = 3;
            // 
            // pictureBoxRefresh
            // 
            pictureBoxRefresh.Image = (Image)resources.GetObject("pictureBoxRefresh.Image");
            pictureBoxRefresh.Location = new Point(173, 88);
            pictureBoxRefresh.Name = "pictureBoxRefresh";
            pictureBoxRefresh.Size = new Size(30, 31);
            pictureBoxRefresh.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxRefresh.TabIndex = 4;
            pictureBoxRefresh.TabStop = false;
            pictureBoxRefresh.Click += pictureBoxRefresh_Click;
            // 
            // btnPanel
            // 
            btnPanel.Location = new Point(3, 88);
            btnPanel.Margin = new Padding(3, 4, 3, 4);
            btnPanel.Name = "btnPanel";
            btnPanel.Size = new Size(105, 31);
            btnPanel.TabIndex = 3;
            btnPanel.Text = "Admin Panel";
            btnPanel.UseVisualStyleBackColor = true;
            btnPanel.Visible = false;
            btnPanel.Click += btnPanel_Click;
            // 
            // imgUserIcon
            // 
            imgUserIcon.BorderStyle = BorderStyle.FixedSingle;
            imgUserIcon.Image = Assets.Resource.icon_user;
            imgUserIcon.Location = new Point(3, 16);
            imgUserIcon.Margin = new Padding(3, 4, 3, 4);
            imgUserIcon.Name = "imgUserIcon";
            imgUserIcon.Padding = new Padding(3, 4, 3, 4);
            imgUserIcon.Size = new Size(55, 63);
            imgUserIcon.SizeMode = PictureBoxSizeMode.StretchImage;
            imgUserIcon.TabIndex = 0;
            imgUserIcon.TabStop = false;
            // 
            // lblEmail
            // 
            lblEmail.AutoSize = true;
            lblEmail.Location = new Point(65, 16);
            lblEmail.Name = "lblEmail";
            lblEmail.Size = new Size(50, 20);
            lblEmail.TabIndex = 2;
            lblEmail.Text = "label1";
            // 
            // lblUsername
            // 
            lblUsername.AutoSize = true;
            lblUsername.Location = new Point(65, 36);
            lblUsername.Name = "lblUsername";
            lblUsername.Size = new Size(50, 20);
            lblUsername.TabIndex = 1;
            lblUsername.Text = "label1";
            // 
            // btnResetFilters
            // 
            btnResetFilters.Location = new Point(3, 517);
            btnResetFilters.Margin = new Padding(3, 4, 3, 4);
            btnResetFilters.Name = "btnResetFilters";
            btnResetFilters.Size = new Size(220, 29);
            btnResetFilters.TabIndex = 15;
            btnResetFilters.Text = "Reset filters";
            btnResetFilters.UseVisualStyleBackColor = true;
            btnResetFilters.Click += btnResetFilters_Click;
            // 
            // StoreView
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1445, 908);
            Controls.Add(panel1);
            Controls.Add(dgvStore);
            Margin = new Padding(3, 4, 3, 4);
            Name = "StoreView";
            Text = "StoreView";
            FormClosed += StoreView_FormClosed;
            Load += StoreView_Load;
            ((System.ComponentModel.ISupportInitialize)dgvStore).EndInit();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxRefresh).EndInit();
            ((System.ComponentModel.ISupportInitialize)imgUserIcon).EndInit();
            ResumeLayout(false);
        }

        #endregion

        public DataGridView dgvStore;
        private Panel panel1;
        private PictureBox imgUserIcon;
        private Label lblEmail;
        private Label lblUsername;
        private Panel panel2;
        private Button btnPanel;
        private TableLayoutPanel tableLayoutPanel1;
        private ComboBox cbSize;
        private Button btnShowCart;
        private Button btnApplyFilters;
        private TextBox txtNameFilter;
        private ComboBox cbCategoryFilter;
        private ComboBox cbBrandFilter;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private ComboBox cbSizeFilter;
        private Button btnResetFilters;
        public Label lblOrdersCounter;
        private DataGridViewButtonColumn Action;
        private PictureBox pictureBoxRefresh;
    }
}