﻿using System.Text.RegularExpressions;

namespace ShopBags.Views
{
    public interface IAuthView
    {
        event EventHandler Login;
        event EventHandler Register;

        string Email { get; }
        string Password { get; }
        string Name { get; }

        void ShowError(string message);
        void ShowInfo(string message);
    }

    public partial class AuthView : Form, IAuthView
    {
        private bool isRegistering = false;

        public AuthView()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        public string Email => txtEmail.Text;
        public string Password => txtPassword.Text;
        public string Name => txtName.Text;

        public event EventHandler Login;
        public event EventHandler Register;

        public void ShowError(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInfo(string message)
        {
            MessageBox.Show(message, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private bool ValidatePassword(string password)
        {
            
            return !string.IsNullOrEmpty(password) && password.Length >= 8;
        }

        private bool ValidateName(string name)
        {
            
            return !string.IsNullOrEmpty(name) && Regex.IsMatch(name, "^[a-zA-Z ]+$");
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (ValidateInputs())
            {
                Login?.Invoke(this, EventArgs.Empty);
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (ValidateInputs())
            {
                Register?.Invoke(this, EventArgs.Empty);
            }
        }

        private bool ValidateInputs()
        {

            if (string.IsNullOrEmpty(Password) || ValidatePassword(Password))
            {
                ShowError("Please enter a valid password with at least 8 characters.");
                return false;
            }

            if (isRegistering && (string.IsNullOrEmpty(Name) || ValidateName(Name)))
            {
                ShowError("Please enter a valid name.");
                return false;
            }

            return true;
        }

        private void AuthView_Load(object sender, EventArgs e)
        {
            txtPassword.PasswordChar = '●';
        }

        private void btnChangeAuthAction_Click(object sender, EventArgs e)
        {
            ChangeAuthAction();
        }

        public void ChangeAuthAction()
        {
            if (!isRegistering)
            {
                btnLogin.Visible = false;
                btnRegister.Visible = true;
                btnChangeAuthAction.Text = "Already have account? Login";
                lblAuthAction.Text = "Register";
                txtName.Visible = true;
                lblName.Visible = true;

                isRegistering = true;
            }
            else
            {
                btnLogin.Visible = true;
                btnRegister.Visible = false;
                btnChangeAuthAction.Text = "Don't have account? Register";
                lblAuthAction.Text = "Login";
                txtName.Visible = false;
                lblName.Visible = false;

                isRegistering = false;
            }
        }

    }
}
